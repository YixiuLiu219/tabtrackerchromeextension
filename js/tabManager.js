



function newStorageIndexLocalStorage(callback){
	var latestWindowIndex = localStorage.getItem('latestWindowIndex')

	// init window data storage index
	if(!latestWindowIndex){
		latestWindowIndex = 1
		localStorage.setItem(latestWindowIndex, 1)
	}
	else{
		latestWindowIndex += 1
		localStorage.setItem(latestWindowIndex, latestWindowIndex)
	}

	//pass the index to the callback
	callback(latestWindowIndex)
}

function newStorageIndex(callback){
	chrome.storage.local.get('latestWindowIndex', function(result){
		var latestWindowIndex = result['latestWindowIndex']
		// init window data storage index
		console.log(chrome.extension.inIncognitoContext)
		if(!chrome.extension.inIncognitoContext){
			if(!latestWindowIndex){
				latestWindowIndex = 1
				chrome.storage.local.set({latestWindowIndex: 1})
			}
			else{
				latestWindowIndex += 1
				chrome.storage.local.set({latestWindowIndex: latestWindowIndex})
			}
		}

		console.log("LATEST"  + latestWindowIndex)

		// a bit delay for incognito to get the latest
		setTimeout(function(){
			chrome.storage.local.get('latestWindowIndex', function(result){
				var latestWindowIndex = result['latestWindowIndex']

				//pass the index to the callback
				callback(latestWindowIndex)
			})
		}, 1000)

	})
}

// transform array of tabs to array of tabs with info I needed
function transformTabs(tabs){
	var newTabs = []
	for(var i in tabs){
		var tab = tabs[i]
		if(/^http/.test(tab.url)){
			newTabs.push({
				title: tab.title,
				url: tab.url
			})
		}
	}
	return newTabs
}

// map window to tab for current chrome session
function storeAllOpenWindows(sessionKey)	{
	chrome.windows.getAll( { populate: true }, function(windows){
		for(var i in windows){
			updateWindow(sessionKey, windows[i])
		}

		installTabChangeSaver()
	})
}


function storeMapping(sessionKey, windowID, tabs, callback) {
	storeObject = {}
	storeObject[sessionKey+"_"+windowID] = tabs
	chrome.storage.local.set(storeObject, callback)
}

function removeMapping(sessionKey, windowID, callback){
	//console.log(sessionKey+"_"+windowID)
	chrome.storage.local.remove(sessionKey+"_"+windowID, callback)
}


function updateWindow(sessionKey, window, callback){
	if(window.tabs.length > 0){
		var tabs = transformTabs(window.tabs)
		storeMapping(sessionKey, window.id, tabs, callback)
	}
	else{
		removeMapping(sessionKey, window.id, callback)
	}
}


function updateWindowByIDUseCurrentSession(windowID, callback) {
	chrome.windows.get(windowID, {populate: true}, function(window){
		chrome.storage.local.get("latestWindowIndex", function(result){
			updateWindow(result["latestWindowIndex"], window, callback)
		})
	})
}

function installTabChangeSaver(){//updateURL, detach, attach, closed

	chrome.tabs.onMoved.addListener(function(tabID, moveInfo){
		updateWindowByIDUseCurrentSession(moveInfo.windowId, function(){ notifyUI(moveInfo.windowId) })
	})

	chrome.tabs.onDetached.addListener(function(tabID, detachInfo){
		updateWindowByIDUseCurrentSession(detachInfo.oldWindowId, function(){ notifyUI(detachInfo.oldWindowId) })
	})

	chrome.tabs.onAttached.addListener(function(tabID, attachInfo){
		updateWindowByIDUseCurrentSession(attachInfo.newWindowId, function(){ notifyUI(attachInfo.newWindowId) })
	})

	chrome.tabs.onUpdated.addListener(function(tabID, changeInfo, tab){
		updateWindowByIDUseCurrentSession(tab.windowId, function(){ notifyUI(tab.windowId) })
	})



	chrome.tabs.onRemoved.addListener(function(tabID, removeInfo){
		if(!removeInfo.isWindowClosing){
			//console.log("not window closing detected")
			updateWindowByIDUseCurrentSession(removeInfo.windowId, function(){ notifyUI(removeInfo.windowId) })
		}
		else{
			//console.log("window closing")
			/*
			chrome.windows.get(removeInfo.windowId, {populate: true}, function(window){
				console.log("got in")
				console.log(window)
				console.log(window.tabs)
				if(window.tabs.length < 1)
					updateWindowByIDUseCurrentSession(removeInfo.windowId, function(){ notifyUI(removeInfo.windowId) })
			})
			*/
		}
	})
}

function notifyUI(windowID){
	chrome.runtime.sendMessage({updateUI: true, windowID: windowID})
}

function clearEmptyWindows(){
	chrome.storage.local.get(function(result){
		for(var field in result){
			if(/^[0-9]+_[0-9]+$/.test(field)){	// match all key with "number_number"
				if(result[field].length <= 0){
					chrome.storage.local.remove(field)
				}
			}
		}
	})
}

//chrome.storage.local.get(function(e){console.log(e)})

setInterval(function(){ clearEmptyWindows() }, 1000)
newStorageIndex(storeAllOpenWindows)
