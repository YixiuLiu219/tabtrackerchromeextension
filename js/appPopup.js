function openAppWindow(tab) {
	chrome.windows.create({
		url: chrome.runtime.getURL("html/app_window.html"),
		type: "popup"
	})
}

chrome.browserAction.onClicked.addListener(openAppWindow)