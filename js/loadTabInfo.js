
function tabInfoCreate(url, title){
	var tabSection = $('<span>').append(
		$('<p>', {}).append($('<a>', { html: title, href: url }))
	)
	return tabSection
}

function openTab(url, incognito, callback){
	chrome.windows.create({
		url: url,
		incognito: incognito
	}, callback)
}

function removeWindow(sessionID, windowID, callback){
	var storeKey = sessionID+"_"+windowID
	chrome.storage.local.remove(storeKey, callback)
}

function removeTab(sessionID, windowID, index, callback){
	var storeKey = sessionID+"_"+windowID
	chrome.storage.local.get(storeKey, function(result){
		var tabList = result[storeKey]
		tabList.splice(index, 1)

		// check if the remove empties the tab array, remove the empty array index if empty
		if(tabList.length > 0) {
			var newStoreObject = {}
			newStoreObject[storeKey] = tabList
			chrome.storage.local.set(newStoreObject, callback)
		}
		else{
			chrome.storage.local.remove(storeKey, callback)
		}
	})
}

function tabDeleteCreate(sessionID, windowID, index){
	return $('button', {id: sessionID+"_"+windowID+"_"+index })
}

function toSectionID(sessionID, windowID){
	return sessionID + "_" + windowID
}

function updateTabList(div, sessionID, windowID, tabs){
	var id = toSectionID(sessionID, windowID)
	var tabListContainerID = id+"_container"
	var tabListContainer = $("#"+tabListContainerID)

	// if the choice is to update to empty tabs, remove this tab list
	if(!tabs || tabs.length <= 0) {
		tabListContainer.remove()
		return
	}

	if(!tabListContainer.length){		// if there isn't a container for this group of tabs, init
		$('<div>', {id: tabListContainerID, class: 'isolated_block'}).appendTo(div)
		tabListContainer = $("#"+tabListContainerID)
	}
	tabListContainer.empty()
	tabListContainer.prepend($("<hr>"))
	var tabUrls = []
	for(var i = 0; i < tabs.length; i++){
		var tab = tabs[i]
		//console.log(tab.title)
		var tabInfo = tabInfoCreate(tab.url, tab.title)
		var tabButtons = tabButtonsCreate(tab.url, sessionID, windowID, i)
		tabListContainer.append(tabInfo)
		tabListContainer.append(tabButtons)
		tabUrls.push(tab.url)

	}
	tabListContainer.prepend(groupButtonsCreate(tabUrls, sessionID, windowID))
}


function groupButtonsCreate(urls, sessionID, windowID){
	var tabButtons = $('<span>').append(
		$('<button>', { html: "Open All", click: function(){ openTab(urls, false) } }),
		$('<button>', { html: "Incognito All", click: function(){ openTab(urls, true) } }),
		$('<button>', { html: "Delete All", click: function(){
				removeWindow(sessionID, windowID, function(){ updateListByWindowIDAndSession(windowID, sessionID) })	// need callback to update ui, since auto update is only for current session, not older sessions
			}
		})
	)
	return tabButtons
}


function tabButtonsCreate(url, sessionID, windowID, tabIndex){
	var tabButtons = $('<span>').append(
		$('<button>', { html: "Open", click: function(){ openTab(url, false) }	}),
		$('<button>', { html: "Incognito", click: function(){ openTab(url, true ) }	}),
		$('<button>', { html: "Delete", click: function(){
				removeTab(sessionID, windowID, tabIndex, function(){ updateListByWindowIDAndSession(windowID, sessionID) })	// need callback to update ui, since auto update is only for current session, not older sessions
			}
		})
	)
	return tabButtons
}

function reloadCurrentSessionListWithDelay(delay){
	setTimeout(function() { populateTabInfoBySession() }, delay)
}

function reloadCurrentSessionListAtInterval(delay){
	setInterval(function() { populateTabInfoBySession(true) }, delay)
}


function getAllTabGroups(callback){
	var tabGroups = {}
	chrome.storage.local.get(function(result){
		for(var field in result){
			if(/^[0-9]+_[0-9]+$/.test(field)){	// match all key with "number_number"
				tabGroups[field] = result[field]
			}
		}
		callback(tabGroups)
	})
}

function getTabGroupsBySession(sessionKey, callback){
	var keyPrefix = sessionKey + "_"
	var tabGroups = {}
	chrome.storage.local.get(function(result){
		for(var field in result){
			if(field.startsWith(keyPrefix)){
				tabGroups[field] = result[field]
			}
		}
		callback(tabGroups)
	})
}

function getTabGroupsByWindow(sessionKey, windowIDList, callback){
	tabGroupKeyList = []
	for(var i in windowIDList){
		tabGroupKeyList.push( sessionKey + " " + windowIDList[i] )
	}
	chrome.storage.local.get(tabGroupKeyList, function(result){
		callback(result)
	})
}

function clearTabListBySession(sessionID){
	$("#window_list > div[id^="+sessionID+"]").remove()
}

function populateTabInfoBySession(removeOld){
	chrome.storage.local.get('latestWindowIndex', function(result){	//get this session
		var sessionKey = result['latestWindowIndex']
		getTabGroupsBySession(sessionKey, function(tabGroup){
			if(removeOld)
				clearTabListBySession(sessionKey)

			populateWithTabGroup(tabGroup)
		})
	})
}

function populateTabInfo(){
	getAllTabGroups(function(tabGroup){
		populateWithTabGroup(tabGroup)
	})
}

function populateWithTabGroup(tabGroup){
	for(var field in tabGroup){
		var sessionID = field.substring(0, field.lastIndexOf('_'))
		var windowID = field.substring(field.lastIndexOf('_')+1)
		updateTabList($("#window_list"), sessionID, windowID, tabGroup[field])
	}
}


/*
div.id = session_win_index
button.onclick = function(){ div.remove(), deleteTab(key, winID, index) }
*/

function deleteTab(sessionKey, windowID, index){

}

function deleteTabGroup(sessionKey, windowID){

}

function updateListByWindowIDUseCurrentSession(windowID){
	chrome.storage.local.get('latestWindowIndex', function(result){	//get this session
		var sessionKey = result['latestWindowIndex']
		var tabStorageID = sessionKey+"_"+windowID
		chrome.storage.local.get(tabStorageID, function(result){
			updateTabList($("#window_list"), sessionKey, windowID, result[tabStorageID])
		})
	})
}

function updateListByWindowIDAndSession(windowID, sessionID){
	var tabStorageID = sessionID+"_"+windowID
	chrome.storage.local.get(tabStorageID, function(result){
		updateTabList($("#window_list"), sessionID, windowID, result[tabStorageID])
	})
}


function installTabChangeListener(){
/*
	chrome.runtime.onMessage.addListener(function(message){
		console.log("Change happened! update the window ID "+ message['windowID'])
		updateListByWindowIDUseCurrentSession(message['windowID'])
	})
*/
/*
	chrome.tabs.onMoved.addListener(function(tabID, moveInfo){
		updateListByWindowIDUseCurrentSession(moveInfo.windowId)
	})

	chrome.tabs.onDetached.addListener(function(tabID, detachInfo){
		updateListByWindowIDUseCurrentSession(detachInfo.oldWindowId)
	})

	chrome.tabs.onAttached.addListener(function(tabID, attachInfo){
		updateListByWindowIDUseCurrentSession(attachInfo.newWindowId)
	})

	chrome.tabs.onUpdated.addListener(function(tabID, changeInfo, tab){
		updateListByWindowIDUseCurrentSession(tab.windowId)
	})

	chrome.tabs.onRemoved.addListener(function(tabID, removeInfo){
		if(!removeInfo.isWindowClosing)
			updateListByWindowIDUseCurrentSession(removeInfo.windowId)
	})
	*/
}

populateTabInfo()
installTabChangeListener()

reloadCurrentSessionListAtInterval(1000)
